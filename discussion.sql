-- [SECTION] Inserting Records
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO artists (name) VALUES ("Taylor Swiff");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");


-- INSERT albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Psy 6", "2012-1-1", 2
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Trip", "1996-1-1", 1
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Fearless", "2008-1-1", 3
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Red", "2012-1-1", 3
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "A Star Is Born", "2018-1-1", 4
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Born This Way", "2011-1-1", 4
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Purpose", "2015-1-1", 5
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Dangerous Woman", "2016-1-1", 6
);

-- INSERT songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Gangnam Style", 253, "K-Pop", 1
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Dundiman", 239, "OPM", 2
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Kisapmata", 259, "OPM", 2
);


INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Fearless", 246, "Pop rock", 3
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "State of Grace", 253, "Rock, alternative rock, arena rock", 4
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Black Eyes", 151, "Rock", 5
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Born This Way", 252, "Electropop", 6
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Sorry", 212, "Dancehall", 7
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Into You", 24, "EDM House", 8
);

-- INSERT playlists
INSERT INTO playlists (user_id, datetime_created) VALUES (

);


-- [SECTION] Selecting Records
-- Display all columns/data for all songs
SELECT * FROM songs;

-- Display only the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display only the title of all OPM songs only
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display the title and length of OPM songs that are more than 4 minutes
SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";


-- [SECTION] Updating Records
-- Update the length of Kundiman to 220 seconds
UPDATE songs SET length = 220 WHERE song_name = "Kundiman";

-- [SECTION] Deleting Records
-- Delete all opm songs that are longer than 4 minutes
DELETE FROM songs WHERE genre = "OPM" AND length > 240;


-- [SECTION] Advanced selection

-- Exclude records
SELECT * FROM songs WHERE id != 5;

-- Greater than
SELECT * FROM songs WHERE id > 4;

-- Greater than or equal
SELECT * FROM songs WHERE id >= 4;

-- Less than or equal to
SELECT * FROM songs WHERE id <= 7;

-- Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 5, 6);

-- Find partial matches (LIKE)
SELECT * FROM songs WHERE song_name LIKE "%e"; -- select from end
SELECT * FROM songs WHERE song_name LIKE "b%"; -- select from start
SELECT * FROM songs WHERE song_name LIKE "%a%"; -- select from inbetwen


-- Sort records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Limit returned records
SELECT * FROM songs ORDER BY song_name DESC LIMIT 3;

-- Offset/skip records
SELECT * FROM songs LIMIT 3 OFFSET 2;


-- Getting distinct records, show all unique values
SELECT DISTINCT genre FROM songs;

-- Count
SELECT COUNT(*) FROM songs WHERE genre = "Dancehall";